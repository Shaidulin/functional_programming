// Implement an immutable deepMerge.
let o1 = {x: {a: "x.y"}}
let o2 = {x: {g: "x.z",c:"df"}}

function deepMerge(dataTarget, dataSource) {
    if (typeof dataTarget !=='object'||typeof dataSource !=='object') throw new Error('Both arguments should be objects');
    const dataReplace = Object.assign({}, dataTarget);
    const data =  Object.assign({}, dataSource);

    let cacheObject = {};
    function merge(dataReplace, data, cacheObject) {
        for (var property in dataReplace) {
            if (data[property] && typeof data[property] === "object") {
                cacheObject[property] = data[property];
                deepMerge(dataReplace[property], data[property], cacheObject[property]);
            }
            cacheObject[property] = Object.assign(
                {},
                dataReplace[property],
                data[property]
            );
        }
        return Object.assign({}, dataReplace, data, cacheObject);
    }
    merge(dataReplace, data, cacheObject);
    return cacheObject;
}


console.log(deepMerge(o1, o2));
console.log(deepMerge(o2, o1));
