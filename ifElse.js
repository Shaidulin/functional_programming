/*
Write a function ifElse.
The function takes a boolean condition and two values of any type.
If the condition is true, the first value is returned. Otherwise – the second.
 */
function ifElse(cond, ifTrue, ifFalse) {
    const resultValue =  cond ? ifTrue: ifFalse;
    return resultValue;
}

console.log(ifElse(true, 1, 2))  // 1
console.log(ifElse(false, 1, 2))// 2
