//Implement an immutable merge (shallow merge).

function merge(o1, o2) {
    if (typeof o1 !=='object'|| typeof o2 !=='object') throw new Error('Both arguments should be objects');
    const resultMerg = Object.assign({},o1,o2);
    return resultMerg;
}

let o1 = {x: "x1"}
let o2 = {y: "y2", x: "x1"}

console.log(merge(o1, o2)) // {y: "y2", x: "x2"}
console.log(merge(o2, o1)) // {y: "y2", x "x1"}
