// Write a functional version for each arithmetic operator in JS.

function add(...values) {
    const operationResult = values.reduce((acc,current)=>acc+current);
    return operationResult;
}

const resultSum = add(1,2,3);

function subtract(...values) {
    const operationResult = values.reduce((acc,current)=>acc-current);
    return operationResult;
}

const resulSubtract = subtract(1,2,3);

function multiply(...values) {
    const operationResult = values.reduce((acc,current)=>acc*current);
    return operationResult;
}

const resulMultiply = multiply(1,2,3);

function divide(...values) {
    const operationResult = values.reduce((acc,current)=>acc/current);
    return operationResult;
}

const resulDevide = divide(1,2,3);
