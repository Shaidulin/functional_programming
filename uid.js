function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function setRandomElemList(targetList,sourceList) {
    const sourceListCount = sourceList.length-1;
    const randomElemList = targetList.map(elTargetList=> {
      const randomIndexSourceList = getRandomInt(0,sourceListCount)
      const randomElemSourceList = sourceList[randomIndexSourceList];
    return randomElemSourceList;
    });
    const randomStr = randomElemList.join('');
    return randomStr;
}

function uid(alphabet, n=1) {
    if (typeof n !=='number') throw new Error('Argument n should be number');
    if (typeof alphabet !=='string') throw new Error('Argument alphabet should be string');

    const sourceList = alphabet.split('');
    const targetList = Array(n).fill('');
    const elemList = setRandomElemList(targetList,sourceList);
    return elemList;
}

const resUid = uid("abc", 3); // One of: "a", "b"

uid("ab", 1) // One of: "a", "b"
uid("ab", 2) // One of: "aa", "ab", "ba", "bb"
uid("ab", 3) // One of: "aaa", "aab", "aba" ...

uid("abc", 1) // One of: "a", "b", "c"
uid("abc", 2) // One of: "aa", "ab", "ac", "ba", "bb", "bc", "ca", "cb", "cc"
uid("abc", 3) // One of: "aaa", "aab", "aac" ...
